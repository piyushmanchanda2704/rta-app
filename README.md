
## Based on
Angular 4+, Angular CLI, TypeScript, Scss, Bootstrap

## Demo

[Live Demo](http://treesflower.com/ng-pi-admin)

## Dependencies

Node.js
Angular cli

## Getting started


npm install

ng serve 

localhost:4200
```

## License
[MIT license](LICENSE)
