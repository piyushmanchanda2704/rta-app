import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Headers} from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class PolicyServicesService {
  readonly rootURL = "https://9u97bp65yb.execute-api.us-east-1.amazonaws.com/test3";
  public responseData ;
  headers = new HttpHeaders({
    'Content-Type' : 'application/json',
"Access-Control-Allow-Origin": "*",
"x-api-key":"2VLdb9BzpB447lSViNoNW1zhHlL1TMp8atXmyaZx"
  });
  constructor(private http: HttpClient) {
  /*  this.headers =  this.headers.append("Access-Control-Allow-Origin", "*");
   this.headers =  this.headers.append("Authorization", "key"); */
  }

   CreateUser(UserData): any {
    return this.http.post(this.rootURL, UserData , {headers : this.headers} );
  } 
 /*  public CreateUser(): Observable<any[]>
  {
    // const url = 'http://localhost:3000/employees';
 
    return this.http.get<any[]>(this.rootURL , { headers : this.headers});
  } */
}
