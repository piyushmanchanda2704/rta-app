import { TestBed, inject } from '@angular/core/testing';

import { PolicyServicesService } from './policy-services.service';

describe('PolicyServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PolicyServicesService]
    });
  });

  it('should be created', inject([PolicyServicesService], (service: PolicyServicesService) => {
    expect(service).toBeTruthy();
  }));
});
