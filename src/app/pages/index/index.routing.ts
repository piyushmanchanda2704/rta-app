import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index.component';
import { PolicyRenewalComponent } from './policy-renewal/policy-renewal.component';
const childRoutes: Routes = [
    {
        path: '',
        component: PolicyRenewalComponent
    },
    {
        path:'bop',
        component:IndexComponent
    }
];

export const routing = RouterModule.forChild(childRoutes);
