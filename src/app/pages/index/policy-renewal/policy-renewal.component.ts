import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {PolicyServicesService} from '../policy-services.service';
import { RootComponent } from '../../../shared/roots/root.component';
import { GlobalService } from '../../../shared/services/global.service';
import swal from 'sweetalert2';
import { Router} from "@angular/router";
@Component({
  selector: 'app-policy-renewal',
  templateUrl: './policy-renewal.component.html',
  styleUrls: ['./policy-renewal.component.scss']
})
export class PolicyRenewalComponent extends RootComponent implements OnInit {

  constructor(private PolicyServicesService : PolicyServicesService , public _globalService: GlobalService , private Router : Router ) { 
    super(_globalService);
    this.renewalForm = this.createFormGroup();
  }
  renewalForm: FormGroup;
  ngOnInit() {
  }
 /*  alert2Error() {
    swal({
      type: 'error',
      title: 'Oops...',
      text: 'Something went wrong!',
    });
  } */

  notification(type , title,value) {
    this.alertMessage(
      {
        type: type,
        title: title,
        value: value
      }
    );
  }

  createFormGroup() {
    return new FormGroup({
      ZipCode: new FormControl('75201', [Validators.required]),
      SICCode: new FormControl('5812', [Validators.required]),
      CLASS_CODE_OF_BUSINESS: new FormControl('09091', [Validators.required]),
      NUMBER_OF_BUILDINGS: new FormControl(12, [Validators.required]),
      NUMBER_OF_BRANCHES: new FormControl(10, [Validators.required]),
      CURRENT_BASE_PREMIUM_AMOUNT: new FormControl(4000, [Validators.required]),
      BUSINESSNAME:new FormControl('ROTHSTEIN, KASS & CO, PLLC', [Validators.required]),
      BUSINESSADDRESS: new FormControl('2525 MCKINNON ST STE 600', [Validators.required]),
      BUSINESSCITY: new FormControl('DALLAS', [Validators.required]),
      BUSINESSSTATE: new FormControl('TX', [Validators.required]),
    });
  }
  onSubmit() {
  let timerStart = new Date ()
    const request = {
      "Webpacket": {
        "ZipCode": "75201",
                                "SICCode": this.renewalForm.value.SICCode,
                                "CLASS_CODE_OF_BUSINESS": this.renewalForm.value.CLASS_CODE_OF_BUSINESS,
                                "NUMBER_OF_BUILDINGS": this.renewalForm.value.NUMBER_OF_BUILDINGS,
                                "NUMBER_OF_BRANCHES": this.renewalForm.value.NUMBER_OF_BRANCHES,
                                "CURRENT_BASE_PREMIUM_AMOUNT": this.renewalForm.value.CURRENT_BASE_PREMIUM_AMOUNT,
                                "BUSINESSNAME": this.renewalForm.value.BUSINESSNAME,
                                "BUSINESSADDRESS": this.renewalForm.value.BUSINESSADDRESS,
                                "BUSINESSCITY": this.renewalForm.value.BUSINESSCITY,
                                "BUSINESSSTATE": this.renewalForm.value.BUSINESSSTATE

      }
    }

    this.PolicyServicesService.CreateUser(request).subscribe(res => { 
      let outTime = new Date();

      this.PolicyServicesService.responseData = res;
      this.Router.navigate(["/pages/index/bop"]);
      this.notification('success' , "Success" , "Data saved Successfully in " + (outTime.getTime() - timerStart.getTime() ) / 1000 + ' seconds' );
    } , err => {
      this.notification('error' , "Error" , "Looks like something went wrong");
    });
  }
}
