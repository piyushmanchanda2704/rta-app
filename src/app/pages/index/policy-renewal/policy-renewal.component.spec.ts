import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyRenewalComponent } from './policy-renewal.component';

describe('PolicyRenewalComponent', () => {
  let component: PolicyRenewalComponent;
  let fixture: ComponentFixture<PolicyRenewalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyRenewalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyRenewalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
