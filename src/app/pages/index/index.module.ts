import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index.component';
import { routing } from './index.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { PolicyRenewalComponent } from './policy-renewal/policy-renewal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {PolicyServicesService} from './policy-services.service';
import { HttpClientModule } from '@angular/common/http'; 
import { Ng5SliderModule } from 'ng5-slider';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgxEchartsModule,
        routing,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        Ng5SliderModule
    ],
    providers: [PolicyServicesService, 
        // {provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS}
       ],
    declarations: [
        IndexComponent,
        PolicyRenewalComponent
    ],
})
export class IndexModule { }
