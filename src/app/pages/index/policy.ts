export let RESPONSE_DATA  = {
    "ZipCode": "75201",
    "SICCode": "5812",
    "CLASS_CODE_OF_BUSINESS": "09091",
    "NUMBER_OF_BUILDINGS": "12",
    "NUMBER_OF_BRANCHES": "10",
    "CURRENT_BASE_PREMIUM_AMOUNT": "4000.00",
    "BUSINESSNAME": "ROTHSTEIN, KASS & CO, PLLC",
    "BUSINESSADDRESS": "2525 MCKINNON ST STE 600",
    "BUSINESSCITY": "DALLAS",
    "BUSINESSSTATE": "TX",
    "NOClassCode": 1,
    "NUM_OF_BLDG_NORM": 3,
    "NUM_OF_CLASS_NORM": 0.25,
    "NUM_OF_LOC_NORM": 2.5,
    "RAGEHOME": "20",
    "RCOLLEGE": "99",
    "RRENTPER": "16",
    "RBURGLARY": "19",
    "RAGR": "1",
    "RDENSITY": "18",
    "RPEDUNOHS": "3",
    "RANK_ZIP_FREQ3": "0",
    "RANK_ZIP_LR3": "6",
    "rank_SIC_freq3": "8",
    "rank_SIC_lr3": "8",
    "BIN": "700115161",
    "INTERCEPT": "1",
    "MORTGAGES_IND": "0",
    "BAD_TRADES_RATIO": "0",
    "BALLATE": "0.060983827",
    "BALTOT_NORM": "192118.4817",
    "COMBINED_ACCOUNT_BAL_NORM": "34954.1712",
    "COMMERCIAL_INTELLISCORE": "48",
    "INQUIRIES_9MOS_NORM": "3.452263822",
    "INTERMEDIATE_TRADES_NORM": "0.863065956",
    "NUMBER_TOTAL_TRADES_NORM": "12.08292338",
    "NUM_EMPL_NORM": "647.2994666",
    "UCC_DEROG_NORM": "9.493725511",
    "UCC_TOT_NORM": "20.71358293",
    "YEARS_IN_FILE": "30",
    "NUM_EMP": "750",
    "Equation": 19.8918,
    "Centile": "23",
    "Decile": "3",
    "Model_Indicated_Deviation": "-0.26",
    "Relative_Adjustment_Low": "-0.3",
    "Relative_Adjustment_High": "-0.2",
    "Action": "Straight through processing",
    "Premium_Adjustment": -1000,
    "New_Premium": 3000,
    "Reasons": "['The demographic characteristics for this risk are less than favorable','The agency distance for this risk is less than favorable']"
}


{
}export let EntileDate = [{
    "entile_Low" :1,
    "Centile_High":10,
     "Decile_Model":1,
      "Indicated_Deviation":'-47%',
          "Relative_Adjustment_Low":-0.4,
              "Relative_Adjustment_High":-0.35,
              'color':'#039C31'

},{
    "entile_Low" :10,
    "Centile_High":20,
     "Decile_Model":2,
      "Indicated_Deviation":'-37%',
          "Relative_Adjustment_Low":-0.35,
              "Relative_Adjustment_High":-0.3,
              "color":'#03B939'

},{
    "entile_Low" :21,
    "Centile_High":30,
     "Decile_Model":3,
      "Indicated_Deviation":'-26%',
          "Relative_Adjustment_Low":-0.3,
              "Relative_Adjustment_High":-0.2,
              'color':"#03DF44",

},{
    "entile_Low" :31,
    "Centile_High":40,
     "Decile_Model":4,
      "Indicated_Deviation":"-15%",
          "Relative_Adjustment_Low":-0.2,
              "Relative_Adjustment_High":-0.1,
              'color':"#03FF4E",

},{
    "entile_Low" :41,
    "Centile_High":50,
     "Decile_Model":5,
      "Indicated_Deviation":'-5%',
          "Relative_Adjustment_Low":-0.1,
              "Relative_Adjustment_High":0,
              'color':"#A4FF8A",

},{
    "entile_Low" :51,
    "Centile_High":60,
     "Decile_Model":6,
      "Indicated_Deviation":'-5%',
          "Relative_Adjustment_Low":0,
              "Relative_Adjustment_High":0.1,
              'color':"#FAB66B",

},{
    "entile_Low" :81,
    "Centile_High":90,
     "Decile_Model":9,
      "Indicated_Deviation":'38%',
          "Relative_Adjustment_Low":0.35,
              "Relative_Adjustment_High":0.4,
              'color':"#FB990B",

},{
    "entile_Low" :91,
    "Centile_High":100,
     "Decile_Model":10,
      "Indicated_Deviation":"48%",
          "Relative_Adjustment_Low":0.45,
              "Relative_Adjustment_High":0.55,
              'color':"#DB0F02",

},]