import { Component, OnInit } from '@angular/core';
import { ChartsService } from '../charts/components/echarts/charts.service';
import {RESPONSE_DATA,EntileDate} from './policy'
import {PolicyServicesService} from './policy-services.service';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ChartsService]
})
export class IndexComponent implements OnInit {
  showloading: boolean = false;
  value: number = 10;
  options: Options = {
    showSelectionBar: true,
    getSelectionBarColor: (value: number): string => {
      if (value <= 3) {
          return '#2AE02A';
      }
      if (value <= 6) {
          return 'yellow';
      }
      if (value <= 9) {
          return 'orange';
      }
      return 'red';
    }
  };

  public AnimationBarOption;
  policyData = RESPONSE_DATA;
  EntileDate = EntileDate;
 /*  reasons = JSON.parse(RESPONSE_DATA.Reasons); */

  constructor(private _chartsService: ChartsService , private PolicyServicesService : PolicyServicesService) { }

  ngOnInit() {
 /*  this.policyData = this.PolicyServicesService.responseData; */
    this.AnimationBarOption = this._chartsService.getAnimationBarOption();
  }
}
